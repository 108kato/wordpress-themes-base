import Swiper from 'swiper';
var UAParser = require("ua-parser-js");
var aboutslider;
var productslider;
export function slider() {
    var parser = new UAParser()
        , result = parser.getResult()
        ;

    aboutslider = new Swiper('#js-about-slider', {
        speed: 1000,
        loop: true,
        effect: "fade",
        autoHeight: true
    });
    $("#js-about-tab li").on("click", function () {
        var index = $("#js-about-tab li").index(this);
        // console.log(index);
        // console.log(aboutslider.realIndex);
        if (!$(this).hasClass("disabled")) {
            if (index !== aboutslider.realIndex) {
                // console.log(index);
                aboutslider.slideTo(index + 1);
            }
        }
    });
    aboutslider.on('slideNextTransitionStart', function () {
        $("#js-about-tab li").removeClass("current").eq(this.realIndex).addClass("current");
        // console.log('slideNextTransitionStart');
        // console.log(this.realIndex);
        if (this.realIndex === 0) {
            $(".slide-1 .slider-men .img, .slide-1 .slider-women .img").css({
                "transform": "translateX(100px)",
                "opacity": 0
            });
            $(".slide-2 .slider-men .img, .slide-2 .slider-women .img").css({
                "transform": "translateX(-100px)",
                "opacity": 0
            });
            $(".slide-3 .slider-men .img, .slide-3 .slider-women .img").css({
                "transform": "translateX(-100px)",
                "opacity": 0
            });
        }
        if (this.realIndex === 1) {
            $(".slide-2 .slider-men .img, .slide-2 .slider-women .img").css({
                "transform": "translateX(100px)",
                "opacity": 0
            });
            $(".slide-1 .slider-men .img, .slide-1 .slider-women .img").css({
                "transform": "translateX(-100px)",
                "opacity": 0
            });
            $(".slide-3 .slider-men .img, .slide-3 .slider-women .img").css({
                "transform": "translateX(-100px)",
                "opacity": 0
            });
        }
        if (this.realIndex === 3) {
            $(".slide-3 .slider-men .img, .slide-3 .slider-women .img").css({
                "transform": "translateX(100px)",
                "opacity": 0
            });
            $(".slide-1 .slider-men .img, .slide-1 .slider-women .img").css({
                "transform": "translateX(-100px)",
                "opacity": 0
            });
            $(".slide-2 .slider-men .img, .slide-2 .slider-women .img").css({
                "transform": "translateX(-100px)",
                "opacity": 0
            });
        }
        // console.log(this.isEnd);
        // if (this.isEnd) {

        // }
        // console.log(this.previousIndex);

    });
    aboutslider.on('slideNextTransitionEnd', function () {
        // console.log('slideNextTransitionEnd');
        if (this.realIndex === 0) {
            $(".slide-1 .slider-men .img, .slide-1 .slider-women .img").css({
                "transform": "translateX(0)",
                "opacity": 1
            });
            $(".slide-2 .slider-men .img, .slide-2 .slider-women .img").css({
                "opacity": 0
            });
            $(".slide-3 .slider-men .img, .slide-3 .slider-women .img").css({
                "opacity": 0
            });
        }
        if (this.realIndex === 1) {
            $(".slide-2 .slider-men .img, .slide-2 .slider-women .img").css({
                "transform": "translateX(0)",
                "opacity": 1
            });
            $(".slide-1 .slider-men .img, .slide-1 .slider-women .img").css({
                "opacity": 0
            });
            $(".slide-3 .slider-men .img, .slide-3 .slider-women .img").css({
                "opacity": 0
            });
        }
        if (this.realIndex === 3) {
            $(".slide-3 .slider-men .img, .slide-3 .slider-women .img").css({
                "transform": "translateX(0)",
                "opacity": 1
            });
            $(".slide-1 .slider-men .img, .slide-1 .slider-women .img").css({
                "opacity": 0
            });
            $(".slide-2 .slider-men .img, .slide-2 .slider-women .img").css({
                "opacity": 0
            });
        }
    });
    aboutslider.on('slidePrevTransitionStart', function () {
        $("#js-about-tab li").removeClass("current").eq(this.realIndex).addClass("current");
        if (this.realIndex === 0) {
            $(".slide-1 .slider-men .img, .slide-1 .slider-women .img").css({
                "transform": "translateX(-100px)",
                "opacity": 0
            });
            $(".slide-2 .slider-men .img, .slide-2 .slider-women .img").css({
                "transform": "translateX(100px)",
                "opacity": 0
            });
            $(".slide-3 .slider-men .img, .slide-3 .slider-women .img").css({
                "transform": "translateX(100px)",
                "opacity": 0
            });
        }
        if (this.realIndex === 1) {
            $(".slide-2 .slider-men .img, .slide-2 .slider-women .img").css({
                "transform": "translateX(-100px)",
                "opacity": 0
            });
            $(".slide-1 .slider-men .img, .slide-1 .slider-women .img").css({
                "transform": "translateX(100px)",
                "opacity": 0
            });
            $(".slide-3 .slider-men .img, .slide-3 .slider-women .img").css({
                "transform": "translateX(100px)",
                "opacity": 0
            });
        }
        if (this.realIndex === 3) {
            $(".slide-3 .slider-men .img, .slide-3 .slider-women .img").css({
                "transform": "translateX(-100px)",
                "opacity": 0
            });
            $(".slide-1 .slider-men .img, .slide-1 .slider-women .img").css({
                "transform": "translateX(100px)",
                "opacity": 0
            });
            $(".slide-2 .slider-men .img, .slide-2 .slider-women .img").css({
                "transform": "translateX(100px)",
                "opacity": 0
            });
        }
    });
    aboutslider.on('slidePrevTransitionEnd', function () {
        // console.log('slidePrevTransitionEnd');
        if (this.realIndex === 0) {
            $(".slide-1 .slider-men .img, .slide-1 .slider-women .img").css({
                "transform": "translateX(0)",
                "opacity": 1
            });
            $(".slide-2 .slider-men .img, .slide-2 .slider-women .img").css({
                "opacity": 0
            });
            $(".slide-3 .slider-men .img, .slide-3 .slider-women .img").css({
                "opacity": 0
            });
        }
        if (this.realIndex === 1) {
            $(".slide-2 .slider-men .img, .slide-2 .slider-women .img").css({
                "transform": "translateX(0)",
                "opacity": 1
            });
            $(".slide-1 .slider-men .img, .slide-1 .slider-women .img").css({
                "opacity": 0
            });
            $(".slide-3 .slider-men .img, .slide-3 .slider-women .img").css({
                "opacity": 0
            });
        }
        if (this.realIndex === 3) {
            $(".slide-3 .slider-men .img, .slide-3 .slider-women .img").css({
                "transform": "translateX(0)",
                "opacity": 1
            });
            $(".slide-1 .slider-men .img, .slide-1 .slider-women .img").css({
                "opacity": 0
            });
            $(".slide-2 .slider-men .img, .slide-2 .slider-women .img").css({
                "opacity": 0
            });
        }
    });

    $(document).on("click", "#js-slide-left", function () {
        aboutslider.slidePrev();
    });
    $(document).on("click", "#js-slide-right", function () {
        aboutslider.slideNext();
    });

    if (result.device.type === "mobile") {
        productslider = new Swiper('.product-img', {
            pagination: {
                el: '.swiper-pagination',
            },
            // speed: 1000,
            // loop: true
            // effect: "fade",
            // autoHeight: true
        });
    }


    var galleryThumbs = new Swiper('#js-modal-slider-thumb', {
        spaceBetween: 1,
        slidesPerView: 14,
        loop: true,
        freeMode: true,
        loopedSlides: 14, //looped slides should be the same
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        centeredSlides: true,
        breakpoints: {
            768: {
                slidesPerView: 5.2
            }
        }
    });
    var galleryTop = new Swiper('#js-modal-slider-main', {
        spaceBetween: 1,
        slidesPerView: 1.5,
        loop: true,
        loopedSlides: 14, //looped slides should be the same
        thumbs: {
            swiper: galleryThumbs,
        },
        centeredSlides: true,
        breakpoints: {
            768: {
                slidesPerView: 1.2
            }
        }
    });
    // galleryTop.on("slideChangeTransitionStart", function () {
    //     console.log("start");
    // });
    // galleryTop.on("slideChangeTransitionEnd", function () {
    //     console.log("end");
    // });
    $(document).on("click", "#js-look-left", function() {
        galleryTop.slidePrev();
    });
    $(document).on("click", "#js-look-right", function () {
        galleryTop.slideNext();
    });
    $(document).on("click", ".swiper-slide-next", function () {
        galleryTop.slideNext();
    });
    $(document).on("click", ".swiper-slide-prev", function() {
        galleryTop.slidePrev();
    });

    $(document).on("click", "#js-modal-wrap .swiper-slide-active .img", function () {
        if (result.device.type !== "mobile") {
            $("#js-modal-wrap").removeClass("is-show");
            $("#js-modal-wrap-women").removeClass("is-show");
            $(".credit-display-area").removeClass("credit-show");
        } else {
            var text = $("#js-modal-wrap .swiper-slide-active .credit-box").prop('outerHTML');
            $(this).parent().parent().parent().parent().find(".credit-display-area").empty().append(text).addClass("credit-show");
        }
    });
    $(document).on("click", "#js-modal-wrap .btn-credit-show", function () {
        var text = $("#js-modal-wrap .swiper-slide-active .credit-box").prop('outerHTML');
        $(this).parent().parent().find(".credit-display-area").empty().append(text).addClass("credit-show");
    });

    var galleryThumbsWomen = new Swiper('#js-modal-slider-thumb-women', {
        spaceBetween: 1,
        slidesPerView: 14,
        loop: true,
        freeMode: true,
        loopedSlides: 14, //looped slides should be the same
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        centeredSlides: true,
        breakpoints: {
            768: {
                slidesPerView: 5.2
            }
        }
    });
    var galleryTopWomen = new Swiper('#js-modal-slider-main-women', {
        spaceBetween: 1,
        slidesPerView: 1.5,
        loop: true,
        loopedSlides: 14, //looped slides should be the same
        thumbs: {
            swiper: galleryThumbsWomen,
        },
        centeredSlides: true,
        breakpoints: {
            768: {
                slidesPerView: 1.2
            }
        }
    });
    $(document).on("click", "#js-look-left-women", function () {
        galleryTopWomen.slidePrev();
    });
    $(document).on("click", "#js-look-right-women", function () {
        galleryTopWomen.slideNext();
    });
    $(document).on("click", ".swiper-slide-next", function () {
        galleryTopWomen.slideNext();
    });
    $(document).on("click", ".swiper-slide-prev", function () {
        galleryTopWomen.slidePrev();
    });

    $(document).on("click", "#js-modal-wrap-women .swiper-slide-active .img", function () {
        if (result.device.type !== "mobile") {
            $("#js-modal-wrap").removeClass("is-show");
            $("#js-modal-wrap-women").removeClass("is-show");
            $(".credit-display-area").removeClass("credit-show");
        } else {
            var text = $("#js-modal-wrap-women .swiper-slide-active .credit-box").prop('outerHTML');
            $(this).parent().parent().parent().parent().find(".credit-display-area").empty().append(text).addClass("credit-show");
        }
    });

    $(document).on("click", ".swiper-slide-active .modal-close", function () {
        $("#js-modal-wrap").removeClass("is-show");
        $("#js-modal-wrap-women").removeClass("is-show");
        $(".credit-display-area").removeClass("credit-show");
    });
    $(document).on("click", ".modal-close.sp", function () {
        $("#js-modal-wrap").removeClass("is-show");
        $("#js-modal-wrap-women").removeClass("is-show");
        $(".credit-display-area").removeClass("credit-show");
    });
    $(document).on("click", "#js-modal-wrap-women .btn-credit-show", function () {
        var text = $("#js-modal-wrap-women .swiper-slide-active .credit-box").prop('outerHTML');
        $(this).parent().parent().find(".credit-display-area").empty().append(text).addClass("credit-show");
    });
    $(document).on("click", ".credit-display-area .modal-close", function () {
        $(".credit-display-area").removeClass("credit-show");
    });
}