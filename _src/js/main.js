// import lazySizes from "lazysizes";
// lazySizes.init();

// import { slider } from "./swiper_setting";
// import {
//     modal
// } from "./modal";
// import {
//     mainVisualSlider
// } from "./mv";

var UAParser = require("ua-parser-js");
// import Swiper from "swiper";
// sub.jsに定義されたJavaScriptを実行する
// hello();
// youtube();
(function ($) {
    var $win = $(window),
        winW = $win.width(),
        winH = $win.height(),
        scrollPos = $win.scrollTop(),
        scrollPosLast = scrollPos,
        screenEnd = winH + scrollPos;

    var timer = 0;

    $win.on({
        "load": function () {
            scrollPos = $win.scrollTop();
            scrollPosLast = scrollPos;
            winH = $win.height();

            dispPgTop();
        },
        "scroll": function () {
            scrollPos = $win.scrollTop();
            screenEnd = winH + scrollPos;

            dispPgTop();
        },
        "resize": function () {
            if (timer > 0) {
                clearTimeout(timer);
            }

            timer = setTimeout(function () {
                winW = $win.width();
                winH = $win.height();
                scrollPos = $win.scrollTop();
                scrollPosLast = scrollPos;
                screenEnd = winH + scrollPos;

                dispPgTop();
            }, 100);
        }
    });


    // スライダー
    if ($(".photo-slider").length) {
        $(".photo-slider li").each(function () {
            var url = $(this).data("url");
            $(this).css({
                background: 'url("' + url + '") center center / cover no-repeat'
            });
        });

        if (!$(".photo-slider li.current").length) {
            $(".photo-slider li:first").addClass("current");
        }

        setInterval(function () {
            var $hidden = $(".photo-slider li.current");
            var $next = $(".photo-slider li.current").next("li");
            if (!$next.length) {
                $next = $(".photo-slider li:first");
            }
            $(".photo-slider li").removeClass("hidden");
            $hidden.removeClass("current").addClass("hidden");
            $next.addClass("current");
        }, 5000);
    }


    // メニュークリック
    $(".menu-bt").on("click", function () {
        $("body").toggleClass("menu");
    });

    $(".g-header .nav a").on("click", function () {
        $("body").removeClass("menu");
    });


    // ページトップの表示処理
    function dispPgTop() {
        if (scrollPos > 160) {
            $("body").addClass("fixed");
            $(".g-footer .page-top").addClass("on");
        } else {
            $("body").removeClass("fixed");
            $(".g-footer .page-top").removeClass("on");
        }
    }


    // ページスクロール
    $(document).on("click", "a[href^='#']", function () {
        var speed = 500;
        var href = $(this).attr("href");
        var target = $(href == "#" || href == "" ? "html" : href);
        var position = target.offset().top;

        position -= $(".g-header").outerHeight();
        if (position < 0) {
            position = 0;
        }

        $("body, html").animate({
            scrollTop: position
        }, speed, "swing");

        return false;
    });
})(jQuery);